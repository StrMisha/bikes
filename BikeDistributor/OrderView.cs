﻿using System.Collections.Generic;

namespace BikeDistributor
{

	public class OrderView
	{
		public string Title { get; set; }
		public string SubTotal { get; set; }

		public string Tax { get; set; }
		public string Total { get; set; }

		public IList<string> ContentItems { get; }

		public void AddItemLine(string itemContent)
		{
			ContentItems.Add(itemContent);
		}

		public OrderView()
		{
			ContentItems = new List<string>();
		}
	}
}
