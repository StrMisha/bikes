﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BikeDistributor
{
	public class Order
	{
		public IList<OrderItem> Items { get; }

		public Order(Company company)
		{
			Company = company;
			Items = new List<OrderItem>();
		}

		public Company Company { get; private set; }

		public void AddItemLine(OrderItem itemLine)
		{
			Items.Add(itemLine);
		}

		public CultureInfo CurrencyCulture
		{
			get
			{
				if (Items.Any())
				{
					return Items[0].Bike.Price.CurrencyCulture;
				}
				return new CultureInfo("en-US");
			}
		}
	}
}
