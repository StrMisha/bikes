﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
	public class Price
	{
		public int Value { get; set; }
		public Currency Currency { get; set; }

		public CultureInfo CurrencyCulture
		{
			get
			{
				switch (Currency)
				{
					case Currency.USD:
						return new CultureInfo("en-US");
					case Currency.GBP:
						return new CultureInfo("en-GB");
					case Currency.EUR:
						return new CultureInfo("fr-FR");
				}
				return new CultureInfo("en-US");
			}
		}

		public Price(int val)
		{
			Currency = Currency.USD;
			Value = val;
		}
	}
}
