﻿
using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BikeDistributor
{
	public static class OrderManager
	{
		public static string GenerateReceipt(Order order, ReceiptFormat format)
		{
			switch (format)
			{
				case ReceiptFormat.Html:
					return HtmlReceipt(order);
				case ReceiptFormat.Text:
					return TextReceipt(order);
				case ReceiptFormat.Pdf:
				case ReceiptFormat.Word:
					throw new NotImplementedException(string.Format("{0} format not supported yet", format.ToString()));
			}

			return string.Empty;
		}

		private static string TextReceipt(Order order)
		{
			var orderView = GetOrderView(order);
			var result = new StringBuilder(string.Format("{0}{1}", orderView.Title, Environment.NewLine));

			foreach (var item in orderView.ContentItems)
			{
				result.AppendLine(string.Format("\t{0}", item));
			}

			result.AppendLine(orderView.SubTotal);
			result.AppendLine(orderView.Tax);
			result.Append(orderView.Total);

			return result.ToString();
		}

		private static string HtmlReceipt(Order order)
		{
			var orderView = GetOrderView(order);
			var doc = new TagBuilder("html");
			var body = new TagBuilder("body");
			var header = new TagBuilder("h1");
			header.SetInnerText(orderView.Title);

			var listTag = new TagBuilder("ul");

			if (order.Items.Any())
			{
				var listItemsHtml = string.Empty;
				foreach (var item in orderView.ContentItems)
				{
					var listItemTag = new TagBuilder("li");
					listItemTag.InnerHtml = item;
					listItemsHtml += listItemTag.ToString(TagRenderMode.Normal);
				}
				listTag.InnerHtml = listItemsHtml;
			}

			var subTotalTag = new TagBuilder("h3");
			subTotalTag.InnerHtml = orderView.SubTotal;

			var taxTag = new TagBuilder("h3");
			taxTag.InnerHtml = orderView.Tax;

			var totalTag = new TagBuilder("h2");
			totalTag.InnerHtml = orderView.Total;


			body.InnerHtml = header.ToString(TagRenderMode.Normal) + listTag + subTotalTag + taxTag + totalTag;
			doc.InnerHtml = body.ToString(TagRenderMode.Normal);

			var result = new StringBuilder(doc.ToString(TagRenderMode.Normal));
			return result.ToString();
		}

		#region Helpers

		private static OrderView  GetOrderView(Order order)
		{
			var totalAmount = 0d;
			var orderView = new OrderView
			{
				Title = string.Format("Order Receipt for {0}", order.Company.Name)
			};

			foreach (var item in order.Items)
			{
				var thisAmount = GetItemAmount(item);
				orderView.AddItemLine(string.Format("{0} x {1} {2} = {3}", item.Quantity, item.Bike.Brand, item.Bike.Model, thisAmount.ToString("C", item.Bike.Price.CurrencyCulture)));
				totalAmount += thisAmount;
			}

			orderView.SubTotal = string.Format("Sub-Total: {0}", totalAmount.ToString("C", order.CurrencyCulture));
			var tax = totalAmount * Constants.TAX_RATE;
			orderView.Tax = string.Format("Tax: {0}", tax.ToString("C", order.CurrencyCulture));

			var total = totalAmount + tax;
			orderView.Total= string.Format("Total: {0}", total.ToString("C", order.CurrencyCulture));
			return orderView;
		}

		private static double GetItemAmount(OrderItem item)
		{
			var thisAmount = item.Quantity * item.Bike.Price.Value* DiscountManager.GetDiscount(item.Bike.Price.Value, item.Quantity).Value;
			return thisAmount;
		}

		#endregion
	}
}
