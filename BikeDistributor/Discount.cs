﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
	public class Discount
	{
		public double Value { get; set; }
		public int PriceThreshold { get; set; }
		public int QuantityThreshold { get; set; }
		public string Code { get; set; }

		public Discount()
		{
			Value = 1.0d;
		}
	}
}
