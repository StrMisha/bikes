﻿using System;
using System.Collections.Generic;

namespace BikeDistributor
{
	public static class DiscountManager
	{
		public static Discount GetDiscount(int priceValue, int quantity)
		{
			if (priceValue >= Constants.OneThousand && quantity >= 20)
			{
				return discountStorage["d1"];
			}

			if (priceValue >= Constants.TwoThousand && quantity >= 10)
			{
				return discountStorage["d2"];
			}

			if (priceValue >= Constants.FiveThousand && quantity >= 5)
			{
				return discountStorage["d3"];
			}

			return new Discount();
		}

		public static void AddDiscountRule()
		{
			throw new NotImplementedException("Need storage for discount rules");
		}

		public static Dictionary<string, Discount> discountStorage = new Dictionary<string, Discount>
		{
			{"d1", new Discount
			{
				PriceThreshold = Constants.OneThousand,
				QuantityThreshold = 20,
				Value = Constants.OneThousandMoreTwentyCoeff
			}},
			{"d2", new Discount
			{
				PriceThreshold = Constants.TwoThousand,
				QuantityThreshold = 10,
				Value = Constants.TwoThousandMoreTenCoeff
			}},
			{"d3", new Discount
			{
				PriceThreshold = Constants.FiveThousand,
				QuantityThreshold = 5,
				Value = Constants.FiveThousandMoreFiveCoeff
			}}
		};
	}
}
