﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
	public enum ReceiptFormat
	{
		Text,
		Html,
		Pdf,
		Word
	}

	public enum Currency
	{
		USD,
		EUR,
		GBP
	}
}
