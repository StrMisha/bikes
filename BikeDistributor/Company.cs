﻿using System.Globalization;

namespace BikeDistributor
{
	public class Company
	{
		public string Name { get; set; }
		public CultureInfo Culture { get; set; }

		public Company(string name, CultureInfo culture = null)
		{
			Name = name;
			Culture = culture ?? CultureInfo.InvariantCulture;
		}
	}
}
