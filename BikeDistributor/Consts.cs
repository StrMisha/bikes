﻿
namespace BikeDistributor
{
	public static class Constants
	{
		public const double TAX_RATE = .0725d;
		public const int OneThousand = 1000;
		public const int TwoThousand = 2000;
		public const int FiveThousand = 5000;
		public const double OneThousandMoreTwentyCoeff = .9d;
		public const double TwoThousandMoreTenCoeff = .8d;
		public const double FiveThousandMoreFiveCoeff = .8d;
	}
}
