﻿namespace BikeDistributor
{
	public class Bike
	{
		public Bike(string brand, string model, Price price)
		{
			Brand = brand;
			Model = model;
			Price = price ?? new Price(0);
		}

		public string Brand { get; private set; }
		public string Model { get; private set; }
		public Price Price { get; set; }
	}
}
