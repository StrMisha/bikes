﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BikeDistributor.Test
{
	[TestClass]
	public class OrderTest
	{
		private readonly static Bike Defy = new Bike("Giant", "Defy 1", new Price(Constants.OneThousand));
		private readonly static Bike Elite = new Bike("Specialized", "Venge Elite", new Price(Constants.TwoThousand));
		private readonly static Bike DuraAce = new Bike("Specialized", "S-Works Venge Dura-Ace", new Price(Constants.FiveThousand));

		[TestMethod]
		public void ReceiptOneDefy()
		{
			var currency = Currency.USD;
			var company = new Company("Anywhere Bike Shop");
			var order = new Order(company);
			order.AddItemLine(new OrderItem(Defy, 1));
			Assert.AreEqual(ResultStatementOneDefy, OrderManager.GenerateReceipt(order, ReceiptFormat.Text));
		}

		private const string ResultStatementOneDefy = @"Order Receipt for Anywhere Bike Shop
	1 x Giant Defy 1 = $1,000.00
Sub-Total: $1,000.00
Tax: $72.50
Total: $1,072.50";

		[TestMethod]
		public void ReceiptOneElite()
		{
			var company = new Company("Anywhere Bike Shop", new CultureInfo("en-US"));
			var order = new Order(company);
			order.AddItemLine(new OrderItem(Elite, 1));
			Assert.AreEqual(ResultStatementOneElite, OrderManager.GenerateReceipt(order, ReceiptFormat.Text));
		}

		private const string ResultStatementOneElite = @"Order Receipt for Anywhere Bike Shop
	1 x Specialized Venge Elite = $2,000.00
Sub-Total: $2,000.00
Tax: $145.00
Total: $2,145.00";

		[TestMethod]
		public void ReceiptOneDuraAce()
		{
			var company = new Company("Anywhere Bike Shop", new CultureInfo("en-US"));
			var order = new Order(company);
			order.AddItemLine(new OrderItem(DuraAce, 1));
			Assert.AreEqual(ResultStatementOneDuraAce, OrderManager.GenerateReceipt(order, ReceiptFormat.Text));
		}

		private const string ResultStatementOneDuraAce = @"Order Receipt for Anywhere Bike Shop
	1 x Specialized S-Works Venge Dura-Ace = $5,000.00
Sub-Total: $5,000.00
Tax: $362.50
Total: $5,362.50";

		[TestMethod]
		public void HtmlReceiptOneDefy()
		{
			var company = new Company("Anywhere Bike Shop", new CultureInfo("en-US"));
			var order = new Order(company);
			order.AddItemLine(new OrderItem(Defy, 1));
			Assert.AreEqual(HtmlResultStatementOneDefy, OrderManager.GenerateReceipt(order, ReceiptFormat.Html));
		}

		private const string HtmlResultStatementOneDefy = @"<html><body><h1>Order Receipt for Anywhere Bike Shop</h1><ul><li>1 x Giant Defy 1 = $1,000.00</li></ul><h3>Sub-Total: $1,000.00</h3><h3>Tax: $72.50</h3><h2>Total: $1,072.50</h2></body></html>";

		[TestMethod]
		public void HtmlReceiptOneElite()
		{
			var company = new Company("Anywhere Bike Shop", new CultureInfo("en-US"));
			var order = new Order(company);
			order.AddItemLine(new OrderItem(Elite, 1));
			Assert.AreEqual(HtmlResultStatementOneElite, OrderManager.GenerateReceipt(order, ReceiptFormat.Html));
		}

		private const string HtmlResultStatementOneElite = @"<html><body><h1>Order Receipt for Anywhere Bike Shop</h1><ul><li>1 x Specialized Venge Elite = $2,000.00</li></ul><h3>Sub-Total: $2,000.00</h3><h3>Tax: $145.00</h3><h2>Total: $2,145.00</h2></body></html>";

		[TestMethod]
		public void HtmlReceiptOneDuraAce()
		{
			var company = new Company("Anywhere Bike Shop", new CultureInfo("en-US"));
			var order = new Order(company);
			order.AddItemLine(new OrderItem(DuraAce, 1));
			Assert.AreEqual(HtmlResultStatementOneDuraAce, OrderManager.GenerateReceipt(order, ReceiptFormat.Html));
		}

		private const string HtmlResultStatementOneDuraAce = @"<html><body><h1>Order Receipt for Anywhere Bike Shop</h1><ul><li>1 x Specialized S-Works Venge Dura-Ace = $5,000.00</li></ul><h3>Sub-Total: $5,000.00</h3><h3>Tax: $362.50</h3><h2>Total: $5,362.50</h2></body></html>";
	}
}


